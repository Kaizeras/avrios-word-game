describe('App', function() {
  it('should have a title', function() {
    browser.get('/');

    expect(browser.getTitle()).toEqual('DEV | Avrios Word Game');
  });
});
