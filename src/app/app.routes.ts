import { IComponentState } from './main';

export const routes: IComponentState[] = [
    { name: 'root', component: 'app' },
    { name: 'root.game', url: '/', component: 'game' },
    { name: 'root.scores', url: '/scores', component: 'scoreboard' }
];
