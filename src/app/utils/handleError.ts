/*
 Oversimplified error handler for failed requests.
*/

export function handleError(err) {
  console.log(err);
  console.error('A HTTP request has failed.');
}
