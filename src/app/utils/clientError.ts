/*
 * Oversimplified error message collection.
 */

export enum ClientError {
  GameStartFailure = 'Game failed to start. Please, check your internet connection.',
  GameEndFailure   = 'Your Score could not get recorded. Please, check your internet connection.'
}
