/**
 * GameLogic is used to handle the business logic of the game.
 */
import { environment } from '../environment';

export class GameLogic {
  public static allowedGameTimeSeconds = environment.GAME_DURATION;

  // Keeps the previous entry
  private static wordStack = [];

  /*
   * Formula max_score = floor(1.95^(n/3))
   */
  public static calculateScore(word: string): number {
    return Math.floor(Math.pow(word.length / 3, environment.POWER_MULTIPLIER));
  }

  public static checkForPenalty(score: number, currentWordEntry: string) {
    let currentScore = score;
    const lastEntry    = GameLogic.wordStack.pop();

    // Compare previous and current entry
    if (lastEntry > currentWordEntry) {

      currentScore -= (lastEntry.length - currentWordEntry.length);
    }

    GameLogic.wordStack.push(currentWordEntry);

    // Check if resulting score is under 0 and adjust it
    if (currentScore < 0) {
      currentScore = 0;
    }

    return currentScore;
  }

  public static validateWord(userWord: string, validWord: string): boolean {
    if (userWord === validWord) {
      // Clear the word stack for the next entry
      GameLogic.wordStack = [];
      return true;
    } else {
      return false;
    }
  }
}
