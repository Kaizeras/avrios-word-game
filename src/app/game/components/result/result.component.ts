import { Component, Input, Output } from 'angular-ts-decorators';

const template = require('./result.component.html');

@Component({
  selector: 'result',
  template
})
export class ResultComponent {
  @Input() previousScore: number;
  @Input() finalScore: number;
  @Input() gameLoading: boolean;
  @Output() onStartGame;

}
