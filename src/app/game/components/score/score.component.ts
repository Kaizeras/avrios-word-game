import { Component, Input } from 'angular-ts-decorators';

const template = require('./score.component.html');
@Component({
  selector: 'score',
  template
})
export class ScoreComponent {
  @Input() currentScore;
}
