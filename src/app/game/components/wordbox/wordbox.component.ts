import { Component, Input, Output } from 'angular-ts-decorators';
import { IOnChangesObject } from 'angular';
import { GameStates } from '../../game.states';
import './wordbox.component.scss';

const template = require('./wordbox.component.html');

@Component({
  selector: 'wordbox',
  template
})
export class WordboxComponent {
  @Input() word: string;
  @Input() clearUserWord: boolean;
  @Input() gameState: GameStates;
  @Output() userWordChanged: (x: any) => string;

  userWord: string;

  constructor() {

  }

  ngOnChanges(changes: IOnChangesObject) {
    if (changes['clearUserWord']) {
      this.userWord = '';

    }

  }

  handleModelChange() {
    this.userWordChanged({
      $event: this.userWord.toLowerCase()
    });
  }

}
