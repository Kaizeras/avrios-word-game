import { Component, Inject, Input, Output } from 'angular-ts-decorators';
import { IIntervalService, IOnChangesObject } from 'angular';
import { GameStates } from '../../game.states';

const template = require('./timer.component.html');

@Component({
  selector: 'timer',
  template
})
export class TimerComponent {

  @Input() gameState: GameStates;
  @Input() countDownSeconds: number;
  @Output() onTimeOver;

  countdownStatusClass: string;

  /*@ngInject*/
  constructor(@Inject('$interval') private interval: IIntervalService) {

  }

  ngOnChanges(changes: IOnChangesObject) {
    if (changes['gameState'] && changes['gameState'].currentValue === GameStates.running) {
      const that              = this;
      const $intervalInstance = this.interval(() => {
        switch (that.countDownSeconds) {

          /*
           I could also use a directive here to better separate the style logic from business logic;
           Similar to ScoreStatusDirective
            */

          case 1:
            // Stop the timer on the last second.
            this.onTimeOver();
            this.interval.cancel($intervalInstance);
            break;
          case 20:
            that.countdownStatusClass = 'text-warning';
            break;
          case 10:
            that.countdownStatusClass = 'text-danger';
            break;

        }

        that.countDownSeconds--;
      }, 1000, 0);
    }

  }
}
