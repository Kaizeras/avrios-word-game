import { Component, Inject } from 'angular-ts-decorators';
import { ScoreService } from '../../services/score.service';
import { IPlayer } from '../../models/player.model';

const template = require('./scoreboard.component.html');
import './scoreboard.component.scss';
@Component({
  selector: 'scoreboard',
  template
})
export class ScoreboardComponent {

  playerScores: IPlayer[];
  playerScoresLoading: boolean;

  /*@ngInject*/
  constructor(@Inject('ScoreService') private scoreService: ScoreService) {
  }

  ngOnInit() {
    this.playerScoresLoading = true;
    this.scoreService.getScores().then((playerScores: any) => {
      this.playerScores = playerScores;
      this.playerScoresLoading = false;
    });

  }

}
