import { Component, Input, Output, ViewChild } from 'angular-ts-decorators';
import './player-handle.component.scss';
const template = require('./player-handle.component.html');

@Component({
  selector: 'player-handle',
  template
})
export class PlayerHandleComponent {
  @Input() gameLoading: boolean;
  @Input() playerHandle: string;
  @Output() playerHandleChanged;
  @Output() onStartGame;

  @ViewChild('player-handle-input') inputBox: HTMLInputElement;

}
