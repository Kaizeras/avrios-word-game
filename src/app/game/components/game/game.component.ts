import { Component, Inject } from 'angular-ts-decorators';
import { GameStates } from '../../game.states';
import { GameSessionService } from '../../services/game-session.service';
import { Player } from '../../models/player.model';
import { ClientError } from '../../../utils/clientError';

import './game.component.scss';
const template = require('./game.component.html');

@Component({
  selector: 'game',
  template
})
export class GameComponent implements ng.IComponentController {
  clearUserWord: boolean;
  gameStates = GameStates;
  currentWord: string;
  countDownSeconds: number;
  currentScore: number;
  currentGameState: GameStates;
  sessionPlayer: Player;
  playerHandle: string;

  errorMessage: ClientError;
  loadingGame: boolean;

  previousScore: number;
  finalScore: number;

  /*@ngInject*/
  constructor(@Inject('GameSessionService') private gameSessionService: GameSessionService) {
  }

  ngOnInit() {
    this.getGameStateProperties();
    this.playerHandle = '';
  }

  ngOnDestroy() {
    this.gameSessionService.init();
  }

  startGame() {
    this.errorMessage = null;
    this.loadingGame  = true;
    this.gameSessionService.start(this.playerHandle).then(() => {
      this.getGameStateProperties();
      this.getCurrentWord();
    }, () => {

      this.errorMessage = ClientError.GameStartFailure;
    });

  }

  endGame() {
    this.loadingGame  = true;
    // Get the previous score in order to present it in the ResultComponent
    this.previousScore = this.sessionPlayer.score;
    this.gameSessionService.end().then((score: number) => {
      this.finalScore = score;
      this.loadingGame  = false;
      this.getGameStateProperties();
    }, () => {
      this.errorMessage = ClientError.GameEndFailure;
    });

  }

  handleUserInput(event: string) {
    this.gameSessionService.validateWord(event);
    this.getScore();
    this.getCurrentWord();
  }

  /*
   * Because AngularJS  does not have proper property binding, I have to reassign the new values of properties
   * from the service with this method. Emitting through $rootScope/$scope is an option, but I feel it is inappropriate
   * for the current use case.
   *
   * Another option would have been to put the game logic in a parent component and move the state downstream
   * but that would constrain me to using the component.
   *
   * A third option would have been to channel the property values through the resolved promise. But
   * considering that the state of the service also changes synchronously, this is more consistent.
   *
   */
  getGameStateProperties() {
    this.countDownSeconds = this.gameSessionService.countDownSeconds;
    this.currentGameState = this.gameSessionService.status;
    this.currentScore     = this.gameSessionService.score;
    this.sessionPlayer    = this.gameSessionService.player;
  }

  getScore() {
    this.currentScore = this.gameSessionService.score;
  }

  getCurrentWord() {
    /**
     * Clear user word currently entered when there's a new current word to be solved.
     */
    if (this.currentWord !== this.gameSessionService.currentWord) {
      this.currentWord   = this.gameSessionService.currentWord;
      this.clearUserWord = !this.clearUserWord;
    }
  }
}
