
describe('Component: App', () => {
  const mod = angular.module('uiRouterNoop', []);
  mod.service('$state', function() { return {}; });
  mod.service('$urlRouter', function() { return {}; });

// in a spec where $state and $urlRouter should be skipped -- ie. a http model
  let $compile;
  let scope;
  let element;
  beforeEach(angular.mock.module('uiRouterNoop'));
  beforeEach(() => {
    angular.mock.module('GameModule');
  });
  beforeEach(angular.mock.inject(($rootScope) => {
    // $compile = $comp;
    // scope    = $rootScope.$new();
    // element  = angular.element(`<game/>`);
    // console.log($comp)
    // $compile(element)(scope);

  }));

  it('Should exist', () => {
    element = angular.element(`<app>`);
    expect(element).toBeDefined();
  });

});
