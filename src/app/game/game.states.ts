/*
  running = Indicates that the game session has started
  init = Indicates that the game is in a pre-start stage
  end = Indicates that the game session has ended
 */
export enum GameStates {
  running,
  init,
  end
}
