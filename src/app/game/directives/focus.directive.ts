import { Directive, Inject } from 'angular-ts-decorators';
import { IAttributes, IAugmentedJQuery, IScope, ITimeoutService } from 'angular';

/**
 *  Focuses an input element
 */
@Directive({
  selector: 'focus'
})
export class FocusDirective {
  /*@ngInject*/
  constructor(private $scope: IScope,
              private $element: IAugmentedJQuery,
              private $attrs: IAttributes,
              @Inject('$timeout') private timeOut: ITimeoutService) {
  }

  $postLink() {
    this.timeOut(() => {
      this.$element[0].focus();
    }, 100);
  }

}
