import { Directive, Inject } from 'angular-ts-decorators';
import { IAttributes, IAugmentedJQuery, IScope, ITimeoutService } from 'angular';

/**
 *  Disables cutting, pasting and opening context menu on element
 */
@Directive({
  selector: 'clipboardDisable'
})
export class ClipboardDisableDirective {
  /*@ngInject*/
  constructor(private $element: IAugmentedJQuery) {
  }

  $postLink() {
    this.$element.on('cut copy paste contextmenu', this.prevent.bind(this));
  }

  prevent(e) {
    e.preventDefault();

  }

}
