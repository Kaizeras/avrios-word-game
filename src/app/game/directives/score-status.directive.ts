import { Directive } from 'angular-ts-decorators';
import { IAugmentedJQuery } from 'angular';

/**
 *  Detects the value of the score and changes its cointainer's class.
 */
@Directive({
  selector: 'scoreStatus'
})
export class ScoreStatusDirective {
  /*@ngInject*/
  constructor(private $element: IAugmentedJQuery) {
    this.$element[0].className = 'text-danger';
  }

  $postLink() {
    this.$element.on('DOMSubtreeModified', this.onScoreChange.bind(this));
  }

  onScoreChange() {
    const score = parseInt(this.$element[0].innerText, 10);
    switch (true) {
      case (score === 0):
        this.$element[0].className = 'text-danger';
        break;
      case (score > 2 && score <= 4):
        this.$element[0].className = 'text-warning';
        break;
      case (score > 5 && score <= 9):
        this.$element[0].className = '';
        break;
      case (score > 10):
        this.$element[0].className = 'text-success';
        break;

    }
  }
}
