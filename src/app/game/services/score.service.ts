import { Inject, Injectable } from 'angular-ts-decorators';
import { IHttpPromise, IHttpResponse, IHttpService } from 'angular';

import { IPlayer, Player } from '../models/player.model';
import { handleError } from '../../utils/handleError';
import { environment } from '../../environment';

/**
 * ScoreService handles the back-end communication for Players
 */

@Injectable('ScoreService')
export class ScoreService {
  baseUrl = `${environment.API_URL}/scores`;

  /*@ngInject*/
  constructor(@Inject('$http') private http: IHttpService) {
  }

  getScores(): IHttpPromise<IPlayer[]> {
    return this.http.get(this.baseUrl).then((res: IHttpResponse<IPlayer[]>) => {
      return res.data.map((playerEntry) => {
        return new Player(playerEntry);
      });
    }, (err) => {
      handleError(err);
      return err;
    });

  }

  getPlayer(playerName: string): IHttpPromise<any> {
    return this.http.get(`${this.baseUrl}?where=name%3D'${playerName}'`).then((res: IHttpResponse<IPlayer[]>) => {
      let player: IPlayer;
      // If Player is not found, create one.
      if (res.data.length === 0) {
        player = new Player({name: playerName});
      } else {
        player = new Player(res.data[0]);

      }
      return player;
    }, (err) => {
      handleError(err);
      return err;
    });
  }

  updateScore(player: IPlayer): IHttpPromise<any> {
    /*
     * Delete the 'exists' property - It's not needed on the server. For more advanced use cases, I might have used a utility
     * method to prepare the response object.
      */
    delete player.exists;
    return this.http.put(`${this.baseUrl}/${player.objectId}`, player).then((res: IHttpResponse<IPlayer>) => {
      return res.data.score;
    }, (err) => {
      handleError(err);
      return err;
    });
  }

  registerScore(player: IPlayer): IHttpPromise<any> {
    /*
     * Delete the 'exists' property - It's not needed on the server. For more advanced use cases, I might have used a utility
     * method to prepare the response object.
      */

    delete player.exists;

    return this.http.post(this.baseUrl, player).then((res: IHttpResponse<IPlayer>) => {
      return res.data.score;
    }, (err) => {
      handleError(err);
      return err;
    });
  }

}
