import { Inject, Injectable } from 'angular-ts-decorators';
import { GameStates } from '../game.states';
import { GameLogic } from '../game-logic';
import { IGuessEntry } from '../models/guess-entry.model';
import { IPlayer } from '../models/player.model';
import { GuessEntryService } from './guess-entry.service';
import { ScoreService } from './score.service';
import { IPromise } from 'angular';

/**
 * GameSessionService used to manage the state of the game session.
 */

@Injectable('GameSessionService')
export class GameSessionService {

  private guessEntries: IGuessEntry[];

  private _countDownSeconds: number;
  get countDownSeconds() {
    return this._countDownSeconds;
  }

  private _score: number;
  get score() {
    return this._score;
  }

  private _currentGuessEntry: IGuessEntry;

  get currentWord() {
    return this._currentGuessEntry.mangledWord;
  }

  private _status: GameStates;
  get status() {
    return this._status;
  }

  private _player: IPlayer;
  get player() {
    return this._player;
  }

  /*@ngInject*/
  constructor(@Inject('GuessEntryService') private guessEntryService: GuessEntryService,
              @Inject('ScoreService') private scoreService: ScoreService) {
    this.init();
  }

  // (Re)Initialize game session
  init() {
    this._status           = GameStates.init;
    this._countDownSeconds = -1;
    this._score            = 0;
  }

  // Start game session
  start(playerName: string): Promise<void> {

    const $entries = this.guessEntryService.getEntries();
    const $player  = this.scoreService.getPlayer(playerName);

    // Using type 'any' in the successCallback
    // because lib.es6.d.ts is missing the find method and it took me too much time to find a workaround
    return Promise.all([$entries, $player]).then((sessionData: any) => {
      this._status           = GameStates.running;
      this._countDownSeconds = GameLogic.allowedGameTimeSeconds;
      this._score            = 0;
      this._player           = sessionData[1] as IPlayer;
      this.guessEntries      = sessionData[0] as IGuessEntry[];
      this.changeCurrentGuessEntry();
    }, (error) => {
      return error;
    });

  }

  // End game session
  end(): IPromise<any> | any {
    // If the player's score is higher than his previous score, update the highest score.
    if (this._player.exists && (this._score > this._player.score)) {
      this._player.score = this._score;
      return this.scoreService.updateScore(this._player).then((scoreData: any) => {
        this._status           = GameStates.end;
        this._countDownSeconds = -1;
        this._score            = 0;

        return scoreData;
      }, (error) => {
        return error;
      });
    }

    // If the player does not exist, register him/her with their current score.
    if (this._player.exists === false) {
      this._player.score = this._score;
      // Using type 'any' in the successCallback
      // because lib.es6.d.ts is missing the find method and it took me too much time to find a workaround
      return this.scoreService.registerScore(this._player).then((scoreData: any) => {
        this._status           = GameStates.end;
        this._countDownSeconds = -1;
        this._score            = 0;

        return scoreData;
      });
    }

    // In case the player exists and hasn't scored a higher score
    const playerSessionScore = this._score;
    this._status             = GameStates.end;
    this._countDownSeconds   = -1;
    this._score              = 0;
    return Promise.resolve(playerSessionScore);

  }

  /**
   * Randomly picks up an entry that is unsolved.
   */
  changeCurrentGuessEntry() {
    const unsolvedEntries   = this.guessEntries.filter((entry) => !entry.solved);
    this._currentGuessEntry = unsolvedEntries[Math.floor(Math.random() * unsolvedEntries.length)];
  }

  /**
   * Called when an entry is valid.
   * Marks the entry as solved and replaces the updated entry in the array.
   */
  completeEntry() {
    this._currentGuessEntry.solved = true;
    this.guessEntries              = this.guessEntries.map((entry) =>
      entry.correctWord === entry.mangledWord ? this._currentGuessEntry : entry);
    this.changeCurrentGuessEntry();
  }

  /**
   * Validates user word and reduces the score if the user has deleted a character.
   * @param {IUserWordChangeEvent} wordEntry
   */
  validateWord(wordEntry: string) {
    this._score = GameLogic.checkForPenalty(this._score, wordEntry);
    if (GameLogic.validateWord(wordEntry, this._currentGuessEntry.correctWord)) {
      this._score += GameLogic.calculateScore(this._currentGuessEntry.correctWord);
      this.completeEntry();
    }

  }

}
