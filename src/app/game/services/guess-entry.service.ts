import { Inject, Injectable } from 'angular-ts-decorators';
import { IHttpPromise, IHttpResponse, IHttpService } from 'angular';

import { handleError } from '../../utils/handleError';
import { GuessEntry } from '../models/guess-entry.model';
import { environment } from '../../environment';

/**
 * GuessEntryService handles the back-end communication for GuessEntries
 */

@Injectable('GuessEntryService')
export class GuessEntryService {
  private baseUrl  = `${environment.API_URL}/guess_entries`;
  // Hardcoding for now
  private pageSize = 100;

  /*@ngInject*/
  constructor(@Inject('$http') private http: IHttpService) {
  }

  getEntries(): IHttpPromise<any> {
    return this.http.get(`${this.baseUrl}?pageSize=${this.pageSize}`).then((res: IHttpResponse<GuessEntry[]>) => {

      return res.data.map((guessEntry) => {
          if (environment.ENABLE_CHEATS) {
            console.log(guessEntry);
          }
          return new GuessEntry(guessEntry);
      });
    }, (err) => {
      handleError(err);
      return err;
    });

  }

}
