export interface IGuessEntry {
  mangledWord: string;
  correctWord: string;
  solved: boolean;
}

export class GuessEntry implements IGuessEntry {
  objectId: string;
  mangledWord: string;
  correctWord: string;
  solved: boolean;

  constructor(data) {
    this.mangledWord = data.mangledWord;
    this.correctWord = data.correctWord;
    this.objectId    = data.objectId;
    this.solved      = false;
  }
}
