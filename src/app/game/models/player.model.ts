export interface IPlayer {
  name: string;
  objectId?: string;
  score?: number;
  exists: boolean;
}

export class Player implements IPlayer {
  name: string;
  exists: boolean;
  objectId?: string;
  score?: number;

  constructor(data) {
    this.name  = data.name.toUpperCase();
    this.score = parseInt(data.score, 10) || 0;

    // Player exists if there is an objectId

    if (data && data.objectId) {
      this.exists   = true;
      this.objectId = data.objectId;
    } else {
      this.exists = false;
    }
  }
}
