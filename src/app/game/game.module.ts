import { NgModule } from 'angular-ts-decorators';
import { GameSessionService } from './services/game-session.service';
import { GameComponent } from './components/game/game.component';
import { WordboxComponent } from './components/wordbox/wordbox.component';
import { TimerComponent } from './components/timer/timer.component';
import { ScoreComponent } from './components/score/score.component';
import { ScoreboardComponent } from './components/scoreboard/scoreboard.component';
import { PlayerHandleComponent } from './components/player-handle/player-handle.component';
import { GuessEntryService } from './services/guess-entry.service';
import { ScoreService } from './services/score.service';
import { ResultComponent } from './components/result/result.component';
import { ScoreStatusDirective } from './directives/score-status.directive';
import { FocusDirective } from './directives/focus.directive';
import { ClipboardDisableDirective } from './directives/clipboard-disable.directive';

/**
 * Contains the word game and its logic
 */

@NgModule({
  id: 'GameModule',
  declarations: [
    GameComponent,
    PlayerHandleComponent,
    WordboxComponent,
    TimerComponent,
    ScoreComponent,
    ScoreboardComponent,
    ResultComponent,
    ScoreStatusDirective,
    FocusDirective,
    ClipboardDisableDirective
  ],
  providers: [
    GameSessionService,
    GuessEntryService,
    ScoreService,
  ]
})
export class GameModule {
}
