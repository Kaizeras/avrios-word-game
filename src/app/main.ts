import { NgModule } from 'angular-ts-decorators';
import { AppComponent } from './app.component';

import { routes } from './app.routes';
import { GameModule } from './game/game.module';
import { CommonModule } from './common/common.module';

export interface IComponentState extends ng.ui.IState {
  name: string;
  component?: string;
  views?: { [name: string]: IComponentState };
}

@NgModule({
  id: 'AppModule',
  imports: [
    'ui.bootstrap',
    'ui.router',
    'ngSanitize',
    GameModule,
    CommonModule
  ],
  declarations: [
    AppComponent,
  ]
})
export class AppModule {
  /*@ngInject*/
  static config($urlRouterProvider: ng.ui.IUrlRouterProvider,
                $stateProvider: ng.ui.IStateProvider) {

    /*
     * Loop over the state definitions and register them
     */
    $urlRouterProvider.otherwise('/');
    routes.forEach((state) => {
      $stateProvider.state(state);
    });
  }

  /*@ngInject*/
  static run($window: ng.IWindowService, $q: ng.IQService) {
    // replace browser Promise to $q in app
    $window.Promise = $q;
  }
}
