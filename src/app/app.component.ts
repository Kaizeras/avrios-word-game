import { Component } from 'angular-ts-decorators';

const template = require('./app.component.html');
import './app.component.scss';
@Component({
  selector: 'app',
  template
})
export class AppComponent {

}
