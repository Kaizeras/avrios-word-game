import * as angular from 'angular';
import { AppModule } from './main';
import { NgModule } from 'angular-ts-decorators';

angular.element(document).ready(() => {
  angular.bootstrap(document, [(AppModule as NgModule).module.name], {strictDi: true});
});
