import { Component } from 'angular-ts-decorators';

const template = require('./navbar.component.html');
import './navbar.component.scss';
@Component({
  selector: 'app-navbar',
  template
})
export class NavbarComponent {

}
