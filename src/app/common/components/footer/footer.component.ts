import { Component } from 'angular-ts-decorators';

const template = require('./footer.component.html');
import './footer.component.scss';
@Component({
  selector: 'app-footer',
  template
})
export class FooterComponent {

}
