import { NgModule } from 'angular-ts-decorators';
import { NavbarComponent } from './components/navbar/navbar.component';
import { FooterComponent } from './components/footer/footer.component';

/**
 * Contains components/directives/filters often used by other modules
 */
@NgModule({
  id: 'CommonModule',
  declarations: [
    NavbarComponent,
    FooterComponent
  ],
  exports: [
    NavbarComponent,
    FooterComponent
  ]
})
export class CommonModule {
}
