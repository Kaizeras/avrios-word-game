<div>
<h1 align="center">Avrios Word Game</h1>

<br/>

<div align="center">
<img src="https://gitlab.com/Kaizeras/avrios-word-game/raw/master/docs/app-screenshot.png" height="200" style="max-width:100%;">
</div>

<h3 align="center">
<a href="http://kaizeras.gitlab.io/avrios-word-game"> Live Version </a>
</h3>
</div>


Project environment used for development and testing:
```
$ node -v
v10.0.0
$ npm -v
5.6.0
$ yarn -v
1.6.0
```
## Getting started

Package versions have been locked to ensure consistent experience when installing dependencies in different environments.
```
yarn start
```

Open your favorite browser and type:
```
http://localhost:4200
```

**Running unit tests:**
```
yarn test
```

**Running e2e tests:**
```
yarn e2e
```

**Building the application**:
``` 
yarn build
```

**How to cheat**

Because of the large number of words, I realized that manual testing can be painful. This is why I'm adding an `ENABLE_CHEATS` option
in the [`environment.ts`](https://gitlab.com/Kaizeras/avrios-word-game/blob/master/src/app/environment.ts) file of the application, causing every word to be logged on the console. This will enable you to easily find the
correct word by filtering by the mangled word in the console.


## Repositories used for setup reference
Before starting the project, I researched several boilerplate repositories, but all of them
had dated dependencies and/or were incomplete. I  decided to combine several different setups and
use Webpack 4, Karma and TypeScript 2.9.2.

- [preboot/angularjs-webpack](https://github.com/preboot/angularjs-webpack)
- [vsternbach/angularjs-typescript-webpack](https://github.com/vsternbach/angularjs-typescript-webpack)
- [vsternbach/tasks-playground](https://github.com/vsternbach/tasks-playground)



## Development approach
During my interview with Roger, he mentioned that one of the current milestones was to get
the existing AngularJS applications to use TypeScript and eventually migrate all products to Angular 2. 
This gave me the idea to setup an app architecture that most closely resembles an Angular 2+ application.
Hypothetically, this project setup and architecture should create  the least friction when migrating from AngularJS
to Angular 2+. 

One of the most challenging parts of doing the project was the setup. There are too many 'gotchas' and
breaking changes between Webpack 2.x (the one I used the most) and Webpack 4.x. I couldn't reuse much.


## Dataset preparation approach
1. Used [1,000 most common US English words](https://gist.github.com/deekayen/4148741) as data source. 
2. Ran the `.txt` file through [TextMechanic](https://textmechanic.com/text-tools/obfuscation-tools/word-scramblerunscrambler/)'s word scrambler to mangle the words.
3. Put together a table with the wordlist and the mangled words in Excel.
4. Filtered all one-letter, two-letter and three-letter entries.
5. Exported to `.csv` and converted to `.json` using [CSVtoJSON](https://www.csvjson.com/csv2json)
6. Tried to bulk `POST` through Backendless, but got throttled down to 100 entries per request.
7. Contemplated writing a script to upload the entries in batches. Scrapped the idea because that's not what the project is about.

## TODO/What can be improved

- File separation between dev, test and production webpack settings.
- Different environment files to setup  dev and production.
- Lazyloaded modules.
- JS build optimization.
- CSS build optimization.
- Better e2e test setup.
- e2e and unit tests.

## Author

| [![Hristo Georgiev](https://github.com/hggeorgiev.png?size=100)](https://github.com/hggeorgiev) |
|---------------------------------------------------------------------------------------------------|
| [Hristo Georgiev](https://github.com/hggeorgiev)                                                |









