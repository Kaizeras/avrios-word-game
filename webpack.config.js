const webpack = require('webpack');
const path = require('path');

const sourcePath = path.join(__dirname, './src');
const destPath = path.join(__dirname, './dist');

const autoprefixer = require('autoprefixer');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
/**
 * Env
 * Get npm lifecycle event to identify the environment
 */
const ENV = process.env.npm_lifecycle_event;
const isTest = ENV === 'test' || ENV === 'test-watch';
const isProd = ENV === 'build';


module.exports = function makeWebpackConfig() {


  /**
   * Config
   * This is the object where all configuration gets set
   */
  var config = {};

  /**
   *  Mode
   *
   */
  config.mode = isProd ? 'production' : 'development';

  /**
   *  Context
   *
   */
  config.context = sourcePath;
  /**
   *  Resolve file extensions
   *
   */
  config.resolve = {
    extensions: ['.js', '.ts', '.css', '.scss'],
    modules: [
      path.resolve(__dirname, 'node_modules'),
      sourcePath
    ]
  };

  /**
   * Entry
   * Should be an empty object if it's generating a test build
   * Karma will set this when it's a test build
   */
  config.entry = isTest ? void 0 : {
    main: sourcePath + '/app/bootstrap.ts',
    vendor: [
      'angular/angular.js',
      '@uirouter/angularjs',
      'angular-sanitize/angular-sanitize.js',
      'angular-ui-bootstrap/dist/ui-bootstrap.js'
    ]
  };

  /**
   * Output
   * Should be an empty object if it's generating a test build
   * Karma will handle setting it up for you when it's a test build
   */


  config.output = isTest ? {} : {
    // Absolute output directory
    path: destPath,

    // Output path from the view of the page
    // Uses webpack-dev-server in development
    publicPath: isProd ? './' : '/',


    // Filename for entry points
    // Only adds hash in build mode
    filename: isProd ? '[name].[hash].js' : '[name].bundle.js',

    // Filename for non-entry points
    // Only adds hash in build mode
    chunkFilename: isProd ? '[name].[hash].js' : '[name].bundle.js'
  };


  /**
   * Devtool
   * Type of sourcemap to use per build type
   */
  if (isTest) {
    config.devtool = 'inline-source-map';
  }
  else if (isProd) {
    config.devtool = 'source-map';
  }
  else {
    config.devtool = 'eval-source-map';
  }


  // Initialize module
  config.module = {
    rules: [
      // TSLINT
      // Reference
      // https://github.com/wbuchwalter/tslint-loader
      // Lint .ts files to ensure good code practices.
      {
        test: /\.ts$/,
        enforce: 'pre',
        use: [
          {
            loader: 'tslint-loader',
            options: {fix: true}
          }
        ]
      },
      {

        // TS LOADER
        // Reference: https://github.com/s-panferov/awesome-typescript-loader
        // Load & transpile TS
        test: /\.ts$/,
        exclude: /node_modules/,
        use: [
          {loader: 'ng-annotate-loader', options: {ngAnnotate: 'ng-annotate-patched'}},
          'awesome-typescript-loader']
      },
      {
        // (S)(C/A)SS LOADER
        // Reference: https://github.com/webpack/css-loader
        // Allow loading css through js
        //
        // Reference: https://github.com/postcss/postcss-loader
        // Postprocess your css with PostCSS plugins

        //mini-css-extract-plugin
        // Reference: https://github.com/webpack-contrib/mini-css-extract-plugin

        test: /\.(sa|sc|c)ss$/,
        use: [
          !isProd ? 'style-loader' : MiniCssExtractPlugin.loader,
          'css-loader',
          'postcss-loader',
          'sass-loader'
        ]
      },

      {
        // ASSET LOADER
        // Copy png, jpg, jpeg, gif, svg, woff, woff2, ttf, eot files to output
        // Rename the file using the asset hash
        // Pass along the updated reference to your code
        // You can add here any file extension you want to get copied to your output
        test: /\.(png|jpg|jpeg|gif|svg|woff|woff2|ttf|eot)$/,
        loader: 'file-loader'
      }, {
        // RAW LOADER
        // Allow loading html through js
        test: /\.html$/,
        loader: 'raw-loader',
        include: [sourcePath],
        exclude: /index\.html$/
      }
    ]
  };


  // Optimization
  config.optimization = {
    splitChunks: {
      name: 'vendor',
      minChunks: Infinity,
      filename: 'vendor.bundle.js'
    }
  };

  if (isProd) {
    config.optimization.minimizer = [
      new UglifyJSPlugin({
        uglifyOptions: {
          compress: {
            warnings: false,
            conditionals: true,
            unused: true,
            comparisons: true,
            sequences: true,
            dead_code: true,
            evaluate: true,
            if_return: true,
            join_vars: true
          },
          output: {
            comments: false
          }
        }
      }),
      new OptimizeCSSAssetsPlugin({})
    ]
  }

  /**
   * Plugins
   */
  config.plugins = [
    new MiniCssExtractPlugin({
      filename: !isProd ? '[name].css' : '[name].[hash].css',
      chunkFilename: !isProd ? '[id].css' : '[id].[hash].css',
    }),
    new webpack.LoaderOptionsPlugin({
      test: /\.scss$/i,
      options: {
        postcss: {
          plugins: [autoprefixer]
        }
      }
    }),
    new webpack.EnvironmentPlugin({
      NODE_ENV: ENV
    }),
    new webpack.NamedModulesPlugin()
  ];


  config.performance = isProd && {
    // IMPORTANT: Sizes are in bytes
    maxEntrypointSize: 500000,
    maxAssetSize: 500000,
    hints: 'warning',
  };

  // Skip rendering index.html in test mode
  if (!isTest) {
    // Reference: https://github.com/ampedandwired/html-webpack-plugin
    // Render index.html
    config.plugins.push(
      new HtmlWebpackPlugin({
        filename:'index.html',
        template: './public/index.html',
        meta: {baseUrl: isProd ? '/avrios-word-game/' : '/'},
        title: isProd ? 'Avrios Word Game' : 'DEV | Avrios Word Game'
      }),


      new webpack.HotModuleReplacementPlugin({disable: isProd})
    )
  }

  // Add build specific plugins
  if (isProd) {
    config.plugins.push(
      // Only emit files when there are no errors
      new webpack.NoEmitOnErrorsPlugin(),


      // Copy assets from the public folder
      // Reference: https://github.com/kevlened/copy-webpack-plugin
      new CopyWebpackPlugin([{
        from: __dirname + '/src/public'
      }]),


      new webpack.LoaderOptionsPlugin({
        minimize: false,
        debug: false
      })
    )
  }

  /**
   * Dev server configuration
   */
  config.devServer = {
    contentBase: './src/public',
    historyApiFallback: true,
    port: 4200,
    compress: isProd,
    inline: !isProd,
    hot: !isProd,
    stats: {
      assets: true,
      children: false,
      chunks: false,
      hash: false,
      modules: false,
      publicPath: false,
      timings: true,
      version: false,
      warnings: true,
      colors: {
        green: '\u001b[32m'
      }
    }
  };


  return config;
}();
