// Reference: http://karma-runner.github.io/0.12/config/configuration-file.html
module.exports = function karmaConfig(config) {
  config.set({
    frameworks: [
      // Reference: https://github.com/karma-runner/karma-jasmine
      // Set framework to jasmine
      'jasmine'
    ],

    reporters: [
      // Reference: https://github.com/mlex/karma-spec-reporter
      // Set reporter to print detailed results to console
      'progress',

      // Reference: https://github.com/karma-runner/karma-coverage
      // Output code coverage files
      'coverage'
    ],

    files: [
      'node_modules/angular/angular.js',
      'node_modules/angular-mocks/angular-mocks.js',
      'node_modules/angular-sanitize/angular-sanitize.js',
      'node_modules/@uirouter/angularjs/release/angular-ui-router.js',
      'node_modules/angular-ui-bootstrap/dist/ui-bootstrap.js',
      'src/app/**/*.spec.ts',
      'src/app/**/*.ts'
    ],


    preprocessors: {
      'src/app/**/*.spec.ts': ['webpack'],
      'src/app/**/*.ts': ['webpack']
    },

    browsers: [
      // Run tests using PhantomJS
      'Chrome'
    ],

    singleRun: false,

    // Configure code coverage reporter
    // coverageReporter: {
    //   dir: 'coverage/',
    //   reporters: [
    //     {type: 'text-summary'},
    //     {type: 'html'}
    //   ]
    // },

    webpack: require('./webpack.config'),

    mime: {
      'text/x-typescript': ['ts', 'tsx']
    }

  });
};
