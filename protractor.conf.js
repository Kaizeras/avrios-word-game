// TODO Use .ts for E2E
exports.config = {
  directConnect: true,
  baseUrl: 'http://localhost:4200',
  specs: ['./src/e2e/**/*.js'],
  capabilities: {
    browserName: 'chrome'
  },
  framework: 'jasmine',
  seleniumAddress: 'http://localhost:4444/wd/hub'
};
